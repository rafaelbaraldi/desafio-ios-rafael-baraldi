//
//  PullRequestsTableViewController.m
//  desafioConcreteObjC
//
//  Created by Rafael Baraldi on 09/04/16.
//  Copyright © 2016 Rafael BAraldi. All rights reserved.
//

#import "PullRequestsTableViewController.h"
#import "BaseManager.h"
#import "PullTableViewCell.h"

@interface PullRequestsTableViewController ()

@end

@implementation PullRequestsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = _pullRequest.itemsDescription;
    
    [self requestPulls];
}

-(void)requestPulls{
    [BaseManager getPullRequests:_pullRequest.owner.login
                            repo:_pullRequest.name
                       onSuccess:^(NSArray* respObj) {
        
        _pullList = respObj;
        
        [self.tableView reloadData];
        
    } onError:^(NSError *err) {
        
        UIAlertController* alert     = [UIAlertController alertControllerWithTitle:@"Erro de conexão"
                                                                           message:[err localizedDescription]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        
        [self presentViewController:alert
                           animated:YES
                         completion:nil];
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return _pullList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PullTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pullCell"
                                                              forIndexPath:indexPath];
    
    if(!cell){
        cell = [[PullTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:@"pullCell"];
    }
    
    GHPullRequestsResponse* pull = [[GHPullRequestsResponse alloc] initWithDictionary:[_pullList objectAtIndex:indexPath.row]];
    
    [cell fillCellWithItem:pull];
    
    return cell;
}


@end
