//
//  RepositoriesTableViewController.h
//  desafioConcreteObjC
//
//  Created by Rafael Baraldi on 09/04/16.
//  Copyright © 2016 Rafael BAraldi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepositoriesDataModels.h"

@interface RepositoriesTableViewController : UITableViewController

@property int page;

@property GHRepositoriesResponse* repos;

@property NSMutableArray* repoList;

@end
