//
//  PullRequestsTableViewController.h
//  desafioConcreteObjC
//
//  Created by Rafael Baraldi on 09/04/16.
//  Copyright © 2016 Rafael BAraldi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRequestsDataModels.h"

@interface PullRequestsTableViewController : UITableViewController

@property GHItems* pullRequest;

@property NSArray* pullList;

@end
