//
//  RepositoriesTableViewController.m
//  desafioConcreteObjC
//
//  Created by Rafael Baraldi on 09/04/16.
//  Copyright © 2016 Rafael BAraldi. All rights reserved.
//

#import "RepositoriesTableViewController.h"
#import "BaseManager.h"
#import "PullRequestsTableViewController.h"
#import "RepoTableViewCell.h"

const int kLoadingCellTag = 1273;

@interface RepositoriesTableViewController ()

@end

@implementation RepositoriesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Github JavaPop";
    
    _repoList = [NSMutableArray new];
    
    _page = 1;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self reposRequest];
}

-(void)reposRequest{
    [BaseManager getRepositories:_page
                        onSucces:^(GHRepositoriesResponse* respObj) {
        
        _repos = respObj;
        
        [_repoList addObjectsFromArray:_repos.items];
        
        [self.tableView reloadData];
        
    } onError:^(NSError *err) {
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Erro de conexão"
                                                                       message:[err localizedDescription]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        
        [self presentViewController:alert
                           animated:YES
                         completion:nil];
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    
    if (_page == 0) {
        return 1;
    }
    
    if (_page < _repos.totalCount) {
        return _repoList.count + 1;
    }
    return _repoList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row < _repoList.count) {
        
        return [self repoCellForIndexPath:indexPath
                                tableView:tableView];
    }
    else {
        
        return [self loadingCell];
    }
}

- (RepoTableViewCell *)repoCellForIndexPath:(NSIndexPath *)indexPath
                                  tableView:(UITableView *)tableView{
    
    RepoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"repoCell"
                                                              forIndexPath:indexPath];
    
    if(!cell){
        cell = [[RepoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:@"repoCell"];
    }
    
    GHItems* repo = (GHItems*)[_repoList objectAtIndex:indexPath.row];
    
    [cell fillCellWithItem:repo];
    
    return cell;
}

- (UITableViewCell *)loadingCell {
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:nil];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activityIndicator.center                   = cell.center;
    [cell addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
    
    cell.tag                                   = kLoadingCellTag;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView
 willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (cell.tag == kLoadingCellTag) {
        _page++;
        [self reposRequest];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue
                 sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"repoSegue"]) {
        PullRequestsTableViewController* destination = (PullRequestsTableViewController*)segue.destinationViewController;
        destination.pullRequest                      = [_repoList objectAtIndex:[self.tableView indexPathForSelectedRow].row];
    }
}


@end
