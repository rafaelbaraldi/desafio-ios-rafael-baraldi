//
//  GHRepositoriesResponse.m
//
//  Created by Marcelo  on 09/04/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GHRepositoriesResponse.h"
#import "GHItems.h"


NSString *const kGHRepositoriesResponseTotalCount = @"total_count";
NSString *const kGHRepositoriesResponseIncompleteResults = @"incomplete_results";
NSString *const kGHRepositoriesResponseItems = @"items";


@interface GHRepositoriesResponse ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GHRepositoriesResponse

@synthesize totalCount = _totalCount;
@synthesize incompleteResults = _incompleteResults;
@synthesize items = _items;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.totalCount = [[self objectOrNilForKey:kGHRepositoriesResponseTotalCount fromDictionary:dict] doubleValue];
            self.incompleteResults = [[self objectOrNilForKey:kGHRepositoriesResponseIncompleteResults fromDictionary:dict] boolValue];
    NSObject *receivedGHItems = [dict objectForKey:kGHRepositoriesResponseItems];
    NSMutableArray *parsedGHItems = [NSMutableArray array];
    if ([receivedGHItems isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedGHItems) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedGHItems addObject:[GHItems modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedGHItems isKindOfClass:[NSDictionary class]]) {
       [parsedGHItems addObject:[GHItems modelObjectWithDictionary:(NSDictionary *)receivedGHItems]];
    }

    self.items = [NSArray arrayWithArray:parsedGHItems];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.totalCount] forKey:kGHRepositoriesResponseTotalCount];
    [mutableDict setValue:[NSNumber numberWithBool:self.incompleteResults] forKey:kGHRepositoriesResponseIncompleteResults];
    NSMutableArray *tempArrayForItems = [NSMutableArray array];
    for (NSObject *subArrayObject in self.items) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForItems addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForItems addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForItems] forKey:kGHRepositoriesResponseItems];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
