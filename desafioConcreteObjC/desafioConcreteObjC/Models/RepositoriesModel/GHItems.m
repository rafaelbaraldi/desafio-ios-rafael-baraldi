//
//  GHItems.m
//
//  Created by Marcelo  on 09/04/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GHItems.h"
#import "GHOwner.h"


NSString *const kGHItemsKeysUrl = @"keys_url";
NSString *const kGHItemsStatusesUrl = @"statuses_url";
NSString *const kGHItemsIssuesUrl = @"issues_url";
NSString *const kGHItemsWatchersCount = @"watchers_count";
NSString *const kGHItemsScore = @"score";
NSString *const kGHItemsIssueEventsUrl = @"issue_events_url";
NSString *const kGHItemsId = @"id";
NSString *const kGHItemsOwner = @"owner";
NSString *const kGHItemsEventsUrl = @"events_url";
NSString *const kGHItemsSubscriptionUrl = @"subscription_url";
NSString *const kGHItemsWatchers = @"watchers";
NSString *const kGHItemsGitCommitsUrl = @"git_commits_url";
NSString *const kGHItemsSubscribersUrl = @"subscribers_url";
NSString *const kGHItemsCloneUrl = @"clone_url";
NSString *const kGHItemsHasWiki = @"has_wiki";
NSString *const kGHItemsPullsUrl = @"pulls_url";
NSString *const kGHItemsUrl = @"url";
NSString *const kGHItemsFork = @"fork";
NSString *const kGHItemsNotificationsUrl = @"notifications_url";
NSString *const kGHItemsDescription = @"description";
NSString *const kGHItemsCollaboratorsUrl = @"collaborators_url";
NSString *const kGHItemsDeploymentsUrl = @"deployments_url";
NSString *const kGHItemsLanguagesUrl = @"languages_url";
NSString *const kGHItemsHasIssues = @"has_issues";
NSString *const kGHItemsCommentsUrl = @"comments_url";
NSString *const kGHItemsPrivate = @"private";
NSString *const kGHItemsSize = @"size";
NSString *const kGHItemsGitTagsUrl = @"git_tags_url";
NSString *const kGHItemsUpdatedAt = @"updated_at";
NSString *const kGHItemsSshUrl = @"ssh_url";
NSString *const kGHItemsName = @"name";
NSString *const kGHItemsArchiveUrl = @"archive_url";
NSString *const kGHItemsOpenIssuesCount = @"open_issues_count";
NSString *const kGHItemsMilestonesUrl = @"milestones_url";
NSString *const kGHItemsBlobsUrl = @"blobs_url";
NSString *const kGHItemsContributorsUrl = @"contributors_url";
NSString *const kGHItemsContentsUrl = @"contents_url";
NSString *const kGHItemsForksCount = @"forks_count";
NSString *const kGHItemsTreesUrl = @"trees_url";
NSString *const kGHItemsMirrorUrl = @"mirror_url";
NSString *const kGHItemsHasDownloads = @"has_downloads";
NSString *const kGHItemsCreatedAt = @"created_at";
NSString *const kGHItemsForksUrl = @"forks_url";
NSString *const kGHItemsSvnUrl = @"svn_url";
NSString *const kGHItemsCommitsUrl = @"commits_url";
NSString *const kGHItemsHomepage = @"homepage";
NSString *const kGHItemsTeamsUrl = @"teams_url";
NSString *const kGHItemsBranchesUrl = @"branches_url";
NSString *const kGHItemsIssueCommentUrl = @"issue_comment_url";
NSString *const kGHItemsMergesUrl = @"merges_url";
NSString *const kGHItemsGitRefsUrl = @"git_refs_url";
NSString *const kGHItemsGitUrl = @"git_url";
NSString *const kGHItemsForks = @"forks";
NSString *const kGHItemsOpenIssues = @"open_issues";
NSString *const kGHItemsHooksUrl = @"hooks_url";
NSString *const kGHItemsHtmlUrl = @"html_url";
NSString *const kGHItemsStargazersUrl = @"stargazers_url";
NSString *const kGHItemsHasPages = @"has_pages";
NSString *const kGHItemsAssigneesUrl = @"assignees_url";
NSString *const kGHItemsLanguage = @"language";
NSString *const kGHItemsCompareUrl = @"compare_url";
NSString *const kGHItemsFullName = @"full_name";
NSString *const kGHItemsTagsUrl = @"tags_url";
NSString *const kGHItemsReleasesUrl = @"releases_url";
NSString *const kGHItemsPushedAt = @"pushed_at";
NSString *const kGHItemsLabelsUrl = @"labels_url";
NSString *const kGHItemsDownloadsUrl = @"downloads_url";
NSString *const kGHItemsDefaultBranch = @"default_branch";
NSString *const kGHItemsStargazersCount = @"stargazers_count";


@interface GHItems ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GHItems

@synthesize keysUrl = _keysUrl;
@synthesize statusesUrl = _statusesUrl;
@synthesize issuesUrl = _issuesUrl;
@synthesize watchersCount = _watchersCount;
@synthesize score = _score;
@synthesize issueEventsUrl = _issueEventsUrl;
@synthesize itemsIdentifier = _itemsIdentifier;
@synthesize owner = _owner;
@synthesize eventsUrl = _eventsUrl;
@synthesize subscriptionUrl = _subscriptionUrl;
@synthesize watchers = _watchers;
@synthesize gitCommitsUrl = _gitCommitsUrl;
@synthesize subscribersUrl = _subscribersUrl;
@synthesize cloneUrl = _cloneUrl;
@synthesize hasWiki = _hasWiki;
@synthesize pullsUrl = _pullsUrl;
@synthesize url = _url;
@synthesize fork = _fork;
@synthesize notificationsUrl = _notificationsUrl;
@synthesize itemsDescription = _itemsDescription;
@synthesize collaboratorsUrl = _collaboratorsUrl;
@synthesize deploymentsUrl = _deploymentsUrl;
@synthesize languagesUrl = _languagesUrl;
@synthesize hasIssues = _hasIssues;
@synthesize commentsUrl = _commentsUrl;
@synthesize privateProperty = _privateProperty;
@synthesize size = _size;
@synthesize gitTagsUrl = _gitTagsUrl;
@synthesize updatedAt = _updatedAt;
@synthesize sshUrl = _sshUrl;
@synthesize name = _name;
@synthesize archiveUrl = _archiveUrl;
@synthesize openIssuesCount = _openIssuesCount;
@synthesize milestonesUrl = _milestonesUrl;
@synthesize blobsUrl = _blobsUrl;
@synthesize contributorsUrl = _contributorsUrl;
@synthesize contentsUrl = _contentsUrl;
@synthesize forksCount = _forksCount;
@synthesize treesUrl = _treesUrl;
@synthesize mirrorUrl = _mirrorUrl;
@synthesize hasDownloads = _hasDownloads;
@synthesize createdAt = _createdAt;
@synthesize forksUrl = _forksUrl;
@synthesize svnUrl = _svnUrl;
@synthesize commitsUrl = _commitsUrl;
@synthesize homepage = _homepage;
@synthesize teamsUrl = _teamsUrl;
@synthesize branchesUrl = _branchesUrl;
@synthesize issueCommentUrl = _issueCommentUrl;
@synthesize mergesUrl = _mergesUrl;
@synthesize gitRefsUrl = _gitRefsUrl;
@synthesize gitUrl = _gitUrl;
@synthesize forks = _forks;
@synthesize openIssues = _openIssues;
@synthesize hooksUrl = _hooksUrl;
@synthesize htmlUrl = _htmlUrl;
@synthesize stargazersUrl = _stargazersUrl;
@synthesize hasPages = _hasPages;
@synthesize assigneesUrl = _assigneesUrl;
@synthesize language = _language;
@synthesize compareUrl = _compareUrl;
@synthesize fullName = _fullName;
@synthesize tagsUrl = _tagsUrl;
@synthesize releasesUrl = _releasesUrl;
@synthesize pushedAt = _pushedAt;
@synthesize labelsUrl = _labelsUrl;
@synthesize downloadsUrl = _downloadsUrl;
@synthesize defaultBranch = _defaultBranch;
@synthesize stargazersCount = _stargazersCount;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.keysUrl = [self objectOrNilForKey:kGHItemsKeysUrl fromDictionary:dict];
            self.statusesUrl = [self objectOrNilForKey:kGHItemsStatusesUrl fromDictionary:dict];
            self.issuesUrl = [self objectOrNilForKey:kGHItemsIssuesUrl fromDictionary:dict];
            self.watchersCount = [[self objectOrNilForKey:kGHItemsWatchersCount fromDictionary:dict] doubleValue];
            self.score = [[self objectOrNilForKey:kGHItemsScore fromDictionary:dict] doubleValue];
            self.issueEventsUrl = [self objectOrNilForKey:kGHItemsIssueEventsUrl fromDictionary:dict];
            self.itemsIdentifier = [[self objectOrNilForKey:kGHItemsId fromDictionary:dict] doubleValue];
            self.owner = [GHOwner modelObjectWithDictionary:[dict objectForKey:kGHItemsOwner]];
            self.eventsUrl = [self objectOrNilForKey:kGHItemsEventsUrl fromDictionary:dict];
            self.subscriptionUrl = [self objectOrNilForKey:kGHItemsSubscriptionUrl fromDictionary:dict];
            self.watchers = [[self objectOrNilForKey:kGHItemsWatchers fromDictionary:dict] doubleValue];
            self.gitCommitsUrl = [self objectOrNilForKey:kGHItemsGitCommitsUrl fromDictionary:dict];
            self.subscribersUrl = [self objectOrNilForKey:kGHItemsSubscribersUrl fromDictionary:dict];
            self.cloneUrl = [self objectOrNilForKey:kGHItemsCloneUrl fromDictionary:dict];
            self.hasWiki = [[self objectOrNilForKey:kGHItemsHasWiki fromDictionary:dict] boolValue];
            self.pullsUrl = [self objectOrNilForKey:kGHItemsPullsUrl fromDictionary:dict];
            self.url = [self objectOrNilForKey:kGHItemsUrl fromDictionary:dict];
            self.fork = [[self objectOrNilForKey:kGHItemsFork fromDictionary:dict] boolValue];
            self.notificationsUrl = [self objectOrNilForKey:kGHItemsNotificationsUrl fromDictionary:dict];
            self.itemsDescription = [self objectOrNilForKey:kGHItemsDescription fromDictionary:dict];
            self.collaboratorsUrl = [self objectOrNilForKey:kGHItemsCollaboratorsUrl fromDictionary:dict];
            self.deploymentsUrl = [self objectOrNilForKey:kGHItemsDeploymentsUrl fromDictionary:dict];
            self.languagesUrl = [self objectOrNilForKey:kGHItemsLanguagesUrl fromDictionary:dict];
            self.hasIssues = [[self objectOrNilForKey:kGHItemsHasIssues fromDictionary:dict] boolValue];
            self.commentsUrl = [self objectOrNilForKey:kGHItemsCommentsUrl fromDictionary:dict];
            self.privateProperty = [[self objectOrNilForKey:kGHItemsPrivate fromDictionary:dict] boolValue];
            self.size = [[self objectOrNilForKey:kGHItemsSize fromDictionary:dict] doubleValue];
            self.gitTagsUrl = [self objectOrNilForKey:kGHItemsGitTagsUrl fromDictionary:dict];
            self.updatedAt = [self objectOrNilForKey:kGHItemsUpdatedAt fromDictionary:dict];
            self.sshUrl = [self objectOrNilForKey:kGHItemsSshUrl fromDictionary:dict];
            self.name = [self objectOrNilForKey:kGHItemsName fromDictionary:dict];
            self.archiveUrl = [self objectOrNilForKey:kGHItemsArchiveUrl fromDictionary:dict];
            self.openIssuesCount = [[self objectOrNilForKey:kGHItemsOpenIssuesCount fromDictionary:dict] doubleValue];
            self.milestonesUrl = [self objectOrNilForKey:kGHItemsMilestonesUrl fromDictionary:dict];
            self.blobsUrl = [self objectOrNilForKey:kGHItemsBlobsUrl fromDictionary:dict];
            self.contributorsUrl = [self objectOrNilForKey:kGHItemsContributorsUrl fromDictionary:dict];
            self.contentsUrl = [self objectOrNilForKey:kGHItemsContentsUrl fromDictionary:dict];
            self.forksCount = [[self objectOrNilForKey:kGHItemsForksCount fromDictionary:dict] doubleValue];
            self.treesUrl = [self objectOrNilForKey:kGHItemsTreesUrl fromDictionary:dict];
            self.mirrorUrl = [self objectOrNilForKey:kGHItemsMirrorUrl fromDictionary:dict];
            self.hasDownloads = [[self objectOrNilForKey:kGHItemsHasDownloads fromDictionary:dict] boolValue];
            self.createdAt = [self objectOrNilForKey:kGHItemsCreatedAt fromDictionary:dict];
            self.forksUrl = [self objectOrNilForKey:kGHItemsForksUrl fromDictionary:dict];
            self.svnUrl = [self objectOrNilForKey:kGHItemsSvnUrl fromDictionary:dict];
            self.commitsUrl = [self objectOrNilForKey:kGHItemsCommitsUrl fromDictionary:dict];
            self.homepage = [self objectOrNilForKey:kGHItemsHomepage fromDictionary:dict];
            self.teamsUrl = [self objectOrNilForKey:kGHItemsTeamsUrl fromDictionary:dict];
            self.branchesUrl = [self objectOrNilForKey:kGHItemsBranchesUrl fromDictionary:dict];
            self.issueCommentUrl = [self objectOrNilForKey:kGHItemsIssueCommentUrl fromDictionary:dict];
            self.mergesUrl = [self objectOrNilForKey:kGHItemsMergesUrl fromDictionary:dict];
            self.gitRefsUrl = [self objectOrNilForKey:kGHItemsGitRefsUrl fromDictionary:dict];
            self.gitUrl = [self objectOrNilForKey:kGHItemsGitUrl fromDictionary:dict];
            self.forks = [[self objectOrNilForKey:kGHItemsForks fromDictionary:dict] doubleValue];
            self.openIssues = [[self objectOrNilForKey:kGHItemsOpenIssues fromDictionary:dict] doubleValue];
            self.hooksUrl = [self objectOrNilForKey:kGHItemsHooksUrl fromDictionary:dict];
            self.htmlUrl = [self objectOrNilForKey:kGHItemsHtmlUrl fromDictionary:dict];
            self.stargazersUrl = [self objectOrNilForKey:kGHItemsStargazersUrl fromDictionary:dict];
            self.hasPages = [[self objectOrNilForKey:kGHItemsHasPages fromDictionary:dict] boolValue];
            self.assigneesUrl = [self objectOrNilForKey:kGHItemsAssigneesUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kGHItemsLanguage fromDictionary:dict];
            self.compareUrl = [self objectOrNilForKey:kGHItemsCompareUrl fromDictionary:dict];
            self.fullName = [self objectOrNilForKey:kGHItemsFullName fromDictionary:dict];
            self.tagsUrl = [self objectOrNilForKey:kGHItemsTagsUrl fromDictionary:dict];
            self.releasesUrl = [self objectOrNilForKey:kGHItemsReleasesUrl fromDictionary:dict];
            self.pushedAt = [self objectOrNilForKey:kGHItemsPushedAt fromDictionary:dict];
            self.labelsUrl = [self objectOrNilForKey:kGHItemsLabelsUrl fromDictionary:dict];
            self.downloadsUrl = [self objectOrNilForKey:kGHItemsDownloadsUrl fromDictionary:dict];
            self.defaultBranch = [self objectOrNilForKey:kGHItemsDefaultBranch fromDictionary:dict];
            self.stargazersCount = [[self objectOrNilForKey:kGHItemsStargazersCount fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.keysUrl forKey:kGHItemsKeysUrl];
    [mutableDict setValue:self.statusesUrl forKey:kGHItemsStatusesUrl];
    [mutableDict setValue:self.issuesUrl forKey:kGHItemsIssuesUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.watchersCount] forKey:kGHItemsWatchersCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.score] forKey:kGHItemsScore];
    [mutableDict setValue:self.issueEventsUrl forKey:kGHItemsIssueEventsUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.itemsIdentifier] forKey:kGHItemsId];
    [mutableDict setValue:[self.owner dictionaryRepresentation] forKey:kGHItemsOwner];
    [mutableDict setValue:self.eventsUrl forKey:kGHItemsEventsUrl];
    [mutableDict setValue:self.subscriptionUrl forKey:kGHItemsSubscriptionUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.watchers] forKey:kGHItemsWatchers];
    [mutableDict setValue:self.gitCommitsUrl forKey:kGHItemsGitCommitsUrl];
    [mutableDict setValue:self.subscribersUrl forKey:kGHItemsSubscribersUrl];
    [mutableDict setValue:self.cloneUrl forKey:kGHItemsCloneUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.hasWiki] forKey:kGHItemsHasWiki];
    [mutableDict setValue:self.pullsUrl forKey:kGHItemsPullsUrl];
    [mutableDict setValue:self.url forKey:kGHItemsUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.fork] forKey:kGHItemsFork];
    [mutableDict setValue:self.notificationsUrl forKey:kGHItemsNotificationsUrl];
    [mutableDict setValue:self.itemsDescription forKey:kGHItemsDescription];
    [mutableDict setValue:self.collaboratorsUrl forKey:kGHItemsCollaboratorsUrl];
    [mutableDict setValue:self.deploymentsUrl forKey:kGHItemsDeploymentsUrl];
    [mutableDict setValue:self.languagesUrl forKey:kGHItemsLanguagesUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.hasIssues] forKey:kGHItemsHasIssues];
    [mutableDict setValue:self.commentsUrl forKey:kGHItemsCommentsUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.privateProperty] forKey:kGHItemsPrivate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kGHItemsSize];
    [mutableDict setValue:self.gitTagsUrl forKey:kGHItemsGitTagsUrl];
    [mutableDict setValue:self.updatedAt forKey:kGHItemsUpdatedAt];
    [mutableDict setValue:self.sshUrl forKey:kGHItemsSshUrl];
    [mutableDict setValue:self.name forKey:kGHItemsName];
    [mutableDict setValue:self.archiveUrl forKey:kGHItemsArchiveUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.openIssuesCount] forKey:kGHItemsOpenIssuesCount];
    [mutableDict setValue:self.milestonesUrl forKey:kGHItemsMilestonesUrl];
    [mutableDict setValue:self.blobsUrl forKey:kGHItemsBlobsUrl];
    [mutableDict setValue:self.contributorsUrl forKey:kGHItemsContributorsUrl];
    [mutableDict setValue:self.contentsUrl forKey:kGHItemsContentsUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.forksCount] forKey:kGHItemsForksCount];
    [mutableDict setValue:self.treesUrl forKey:kGHItemsTreesUrl];
    [mutableDict setValue:self.mirrorUrl forKey:kGHItemsMirrorUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.hasDownloads] forKey:kGHItemsHasDownloads];
    [mutableDict setValue:self.createdAt forKey:kGHItemsCreatedAt];
    [mutableDict setValue:self.forksUrl forKey:kGHItemsForksUrl];
    [mutableDict setValue:self.svnUrl forKey:kGHItemsSvnUrl];
    [mutableDict setValue:self.commitsUrl forKey:kGHItemsCommitsUrl];
    [mutableDict setValue:self.homepage forKey:kGHItemsHomepage];
    [mutableDict setValue:self.teamsUrl forKey:kGHItemsTeamsUrl];
    [mutableDict setValue:self.branchesUrl forKey:kGHItemsBranchesUrl];
    [mutableDict setValue:self.issueCommentUrl forKey:kGHItemsIssueCommentUrl];
    [mutableDict setValue:self.mergesUrl forKey:kGHItemsMergesUrl];
    [mutableDict setValue:self.gitRefsUrl forKey:kGHItemsGitRefsUrl];
    [mutableDict setValue:self.gitUrl forKey:kGHItemsGitUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.forks] forKey:kGHItemsForks];
    [mutableDict setValue:[NSNumber numberWithDouble:self.openIssues] forKey:kGHItemsOpenIssues];
    [mutableDict setValue:self.hooksUrl forKey:kGHItemsHooksUrl];
    [mutableDict setValue:self.htmlUrl forKey:kGHItemsHtmlUrl];
    [mutableDict setValue:self.stargazersUrl forKey:kGHItemsStargazersUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.hasPages] forKey:kGHItemsHasPages];
    [mutableDict setValue:self.assigneesUrl forKey:kGHItemsAssigneesUrl];
    [mutableDict setValue:self.language forKey:kGHItemsLanguage];
    [mutableDict setValue:self.compareUrl forKey:kGHItemsCompareUrl];
    [mutableDict setValue:self.fullName forKey:kGHItemsFullName];
    [mutableDict setValue:self.tagsUrl forKey:kGHItemsTagsUrl];
    [mutableDict setValue:self.releasesUrl forKey:kGHItemsReleasesUrl];
    [mutableDict setValue:self.pushedAt forKey:kGHItemsPushedAt];
    [mutableDict setValue:self.labelsUrl forKey:kGHItemsLabelsUrl];
    [mutableDict setValue:self.downloadsUrl forKey:kGHItemsDownloadsUrl];
    [mutableDict setValue:self.defaultBranch forKey:kGHItemsDefaultBranch];
    [mutableDict setValue:[NSNumber numberWithDouble:self.stargazersCount] forKey:kGHItemsStargazersCount];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
