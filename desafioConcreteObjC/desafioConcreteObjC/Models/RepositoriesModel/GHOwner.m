//
//  GHOwner.m
//
//  Created by Marcelo  on 09/04/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GHOwner.h"


NSString *const kGHOwnerId = @"id";
NSString *const kGHOwnerOrganizationsUrl = @"organizations_url";
NSString *const kGHOwnerReceivedEventsUrl = @"received_events_url";
NSString *const kGHOwnerFollowingUrl = @"following_url";
NSString *const kGHOwnerLogin = @"login";
NSString *const kGHOwnerSubscriptionsUrl = @"subscriptions_url";
NSString *const kGHOwnerAvatarUrl = @"avatar_url";
NSString *const kGHOwnerUrl = @"url";
NSString *const kGHOwnerType = @"type";
NSString *const kGHOwnerReposUrl = @"repos_url";
NSString *const kGHOwnerHtmlUrl = @"html_url";
NSString *const kGHOwnerEventsUrl = @"events_url";
NSString *const kGHOwnerSiteAdmin = @"site_admin";
NSString *const kGHOwnerStarredUrl = @"starred_url";
NSString *const kGHOwnerGistsUrl = @"gists_url";
NSString *const kGHOwnerGravatarId = @"gravatar_id";
NSString *const kGHOwnerFollowersUrl = @"followers_url";


@interface GHOwner ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GHOwner

@synthesize ownerIdentifier = _ownerIdentifier;
@synthesize organizationsUrl = _organizationsUrl;
@synthesize receivedEventsUrl = _receivedEventsUrl;
@synthesize followingUrl = _followingUrl;
@synthesize login = _login;
@synthesize subscriptionsUrl = _subscriptionsUrl;
@synthesize avatarUrl = _avatarUrl;
@synthesize url = _url;
@synthesize type = _type;
@synthesize reposUrl = _reposUrl;
@synthesize htmlUrl = _htmlUrl;
@synthesize eventsUrl = _eventsUrl;
@synthesize siteAdmin = _siteAdmin;
@synthesize starredUrl = _starredUrl;
@synthesize gistsUrl = _gistsUrl;
@synthesize gravatarId = _gravatarId;
@synthesize followersUrl = _followersUrl;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.ownerIdentifier = [[self objectOrNilForKey:kGHOwnerId fromDictionary:dict] doubleValue];
            self.organizationsUrl = [self objectOrNilForKey:kGHOwnerOrganizationsUrl fromDictionary:dict];
            self.receivedEventsUrl = [self objectOrNilForKey:kGHOwnerReceivedEventsUrl fromDictionary:dict];
            self.followingUrl = [self objectOrNilForKey:kGHOwnerFollowingUrl fromDictionary:dict];
            self.login = [self objectOrNilForKey:kGHOwnerLogin fromDictionary:dict];
            self.subscriptionsUrl = [self objectOrNilForKey:kGHOwnerSubscriptionsUrl fromDictionary:dict];
            self.avatarUrl = [self objectOrNilForKey:kGHOwnerAvatarUrl fromDictionary:dict];
            self.url = [self objectOrNilForKey:kGHOwnerUrl fromDictionary:dict];
            self.type = [self objectOrNilForKey:kGHOwnerType fromDictionary:dict];
            self.reposUrl = [self objectOrNilForKey:kGHOwnerReposUrl fromDictionary:dict];
            self.htmlUrl = [self objectOrNilForKey:kGHOwnerHtmlUrl fromDictionary:dict];
            self.eventsUrl = [self objectOrNilForKey:kGHOwnerEventsUrl fromDictionary:dict];
            self.siteAdmin = [[self objectOrNilForKey:kGHOwnerSiteAdmin fromDictionary:dict] boolValue];
            self.starredUrl = [self objectOrNilForKey:kGHOwnerStarredUrl fromDictionary:dict];
            self.gistsUrl = [self objectOrNilForKey:kGHOwnerGistsUrl fromDictionary:dict];
            self.gravatarId = [self objectOrNilForKey:kGHOwnerGravatarId fromDictionary:dict];
            self.followersUrl = [self objectOrNilForKey:kGHOwnerFollowersUrl fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.ownerIdentifier] forKey:kGHOwnerId];
    [mutableDict setValue:self.organizationsUrl forKey:kGHOwnerOrganizationsUrl];
    [mutableDict setValue:self.receivedEventsUrl forKey:kGHOwnerReceivedEventsUrl];
    [mutableDict setValue:self.followingUrl forKey:kGHOwnerFollowingUrl];
    [mutableDict setValue:self.login forKey:kGHOwnerLogin];
    [mutableDict setValue:self.subscriptionsUrl forKey:kGHOwnerSubscriptionsUrl];
    [mutableDict setValue:self.avatarUrl forKey:kGHOwnerAvatarUrl];
    [mutableDict setValue:self.url forKey:kGHOwnerUrl];
    [mutableDict setValue:self.type forKey:kGHOwnerType];
    [mutableDict setValue:self.reposUrl forKey:kGHOwnerReposUrl];
    [mutableDict setValue:self.htmlUrl forKey:kGHOwnerHtmlUrl];
    [mutableDict setValue:self.eventsUrl forKey:kGHOwnerEventsUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.siteAdmin] forKey:kGHOwnerSiteAdmin];
    [mutableDict setValue:self.starredUrl forKey:kGHOwnerStarredUrl];
    [mutableDict setValue:self.gistsUrl forKey:kGHOwnerGistsUrl];
    [mutableDict setValue:self.gravatarId forKey:kGHOwnerGravatarId];
    [mutableDict setValue:self.followersUrl forKey:kGHOwnerFollowersUrl];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
