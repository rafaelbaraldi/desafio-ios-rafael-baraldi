//
//  GHBase.m
//
//  Created by Marcelo  on 09/04/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GHBase.h"
#import "GHRepo.h"
#import "GHUser.h"


NSString *const kGHBaseRef = @"ref";
NSString *const kGHBaseLabel = @"label";
NSString *const kGHBaseSha = @"sha";
NSString *const kGHBaseRepo = @"repo";
NSString *const kGHBaseUser = @"user";


@interface GHBase ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GHBase

@synthesize ref = _ref;
@synthesize label = _label;
@synthesize sha = _sha;
@synthesize repo = _repo;
@synthesize user = _user;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.ref = [self objectOrNilForKey:kGHBaseRef fromDictionary:dict];
            self.label = [self objectOrNilForKey:kGHBaseLabel fromDictionary:dict];
            self.sha = [self objectOrNilForKey:kGHBaseSha fromDictionary:dict];
            self.repo = [GHRepo modelObjectWithDictionary:[dict objectForKey:kGHBaseRepo]];
            self.user = [GHUser modelObjectWithDictionary:[dict objectForKey:kGHBaseUser]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.ref forKey:kGHBaseRef];
    [mutableDict setValue:self.label forKey:kGHBaseLabel];
    [mutableDict setValue:self.sha forKey:kGHBaseSha];
    [mutableDict setValue:[self.repo dictionaryRepresentation] forKey:kGHBaseRepo];
    [mutableDict setValue:[self.user dictionaryRepresentation] forKey:kGHBaseUser];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
