//
//  GHLinks.m
//
//  Created by Marcelo  on 09/04/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GHLinks.h"
#import "GHComments.h"
#import "GHIssue.h"
#import "GHCommits.h"
#import "GHHtml.h"
#import "GHStatuses.h"
#import "GHSelfClass.h"
#import "GHReviewComment.h"
#import "GHReviewComments.h"


NSString *const kGHLinksComments = @"comments";
NSString *const kGHLinksIssue = @"issue";
NSString *const kGHLinksCommits = @"commits";
NSString *const kGHLinksHtml = @"html";
NSString *const kGHLinksStatuses = @"statuses";
NSString *const kGHLinksSelf = @"self";
NSString *const kGHLinksReviewComment = @"review_comment";
NSString *const kGHLinksReviewComments = @"review_comments";


@interface GHLinks ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GHLinks

@synthesize comments = _comments;
@synthesize issue = _issue;
@synthesize commits = _commits;
@synthesize html = _html;
@synthesize statuses = _statuses;
@synthesize linksSelf = _linksSelf;
@synthesize reviewComment = _reviewComment;
@synthesize reviewComments = _reviewComments;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.comments = [GHComments modelObjectWithDictionary:[dict objectForKey:kGHLinksComments]];
            self.issue = [GHIssue modelObjectWithDictionary:[dict objectForKey:kGHLinksIssue]];
            self.commits = [GHCommits modelObjectWithDictionary:[dict objectForKey:kGHLinksCommits]];
            self.html = [GHHtml modelObjectWithDictionary:[dict objectForKey:kGHLinksHtml]];
            self.statuses = [GHStatuses modelObjectWithDictionary:[dict objectForKey:kGHLinksStatuses]];
            self.linksSelf = [GHSelfClass modelObjectWithDictionary:[dict objectForKey:kGHLinksSelf]];
            self.reviewComment = [GHReviewComment modelObjectWithDictionary:[dict objectForKey:kGHLinksReviewComment]];
            self.reviewComments = [GHReviewComments modelObjectWithDictionary:[dict objectForKey:kGHLinksReviewComments]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.comments dictionaryRepresentation] forKey:kGHLinksComments];
    [mutableDict setValue:[self.issue dictionaryRepresentation] forKey:kGHLinksIssue];
    [mutableDict setValue:[self.commits dictionaryRepresentation] forKey:kGHLinksCommits];
    [mutableDict setValue:[self.html dictionaryRepresentation] forKey:kGHLinksHtml];
    [mutableDict setValue:[self.statuses dictionaryRepresentation] forKey:kGHLinksStatuses];
    [mutableDict setValue:[self.linksSelf dictionaryRepresentation] forKey:kGHLinksSelf];
    [mutableDict setValue:[self.reviewComment dictionaryRepresentation] forKey:kGHLinksReviewComment];
    [mutableDict setValue:[self.reviewComments dictionaryRepresentation] forKey:kGHLinksReviewComments];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
