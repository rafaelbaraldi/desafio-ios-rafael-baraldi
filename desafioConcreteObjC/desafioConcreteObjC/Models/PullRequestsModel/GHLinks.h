//
//  GHLinks.h
//
//  Created by Marcelo  on 09/04/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GHComments, GHIssue, GHCommits, GHHtml, GHStatuses, GHSelfClass, GHReviewComment, GHReviewComments;

@interface GHLinks : NSObject

@property (nonatomic, strong) GHComments *comments;
@property (nonatomic, strong) GHIssue *issue;
@property (nonatomic, strong) GHCommits *commits;
@property (nonatomic, strong) GHHtml *html;
@property (nonatomic, strong) GHStatuses *statuses;
@property (nonatomic, strong) GHSelfClass *linksSelf;
@property (nonatomic, strong) GHReviewComment *reviewComment;
@property (nonatomic, strong) GHReviewComments *reviewComments;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
