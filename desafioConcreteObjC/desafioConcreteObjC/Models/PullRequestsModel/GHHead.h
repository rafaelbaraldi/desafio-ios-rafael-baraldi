//
//  GHHead.h
//
//  Created by Marcelo  on 09/04/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepositoriesDataModels.h"

@class GHUser;

@interface GHHead : NSObject

@property (nonatomic, strong) NSString *ref;
@property (nonatomic, strong) NSString *label;
@property (nonatomic, strong) NSString *sha;
@property (nonatomic, strong) GHItems* repo;
@property (nonatomic, strong) GHUser *user;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
