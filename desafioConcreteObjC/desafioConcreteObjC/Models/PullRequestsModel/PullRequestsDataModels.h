//
//  DataModels.h
//
//  Created by Marcelo  on 09/04/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GHBase.h"#import "GHComments.h"#import "GHIssue.h"#import "GHPullRequestsResponse.h"#import "GHHead.h"#import "GHOwner.h"#import "GHCommits.h"#import "GHLinks.h"#import "GHSelfClass.h"#import "GHReviewComment.h"#import "GHReviewComments.h"#import "GHRepo.h"#import "GHStatuses.h"#import "GHUser.h"#import "GHHtml.h"