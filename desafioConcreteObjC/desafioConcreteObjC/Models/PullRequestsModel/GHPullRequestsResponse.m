//
//  GHPullRequestsResponse.m
//
//  Created by Marcelo  on 09/04/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GHPullRequestsResponse.h"
#import "GHBase.h"
#import "GHUser.h"
#import "GHHead.h"
#import "GHLinks.h"


NSString *const kGHPullRequestsResponseMilestone = @"milestone";
NSString *const kGHPullRequestsResponseLocked = @"locked";
NSString *const kGHPullRequestsResponseTitle = @"title";
NSString *const kGHPullRequestsResponseUrl = @"url";
NSString *const kGHPullRequestsResponseCommitsUrl = @"commits_url";
NSString *const kGHPullRequestsResponseMergeCommitSha = @"merge_commit_sha";
NSString *const kGHPullRequestsResponseReviewCommentUrl = @"review_comment_url";
NSString *const kGHPullRequestsResponseUpdatedAt = @"updated_at";
NSString *const kGHPullRequestsResponseBase = @"base";
NSString *const kGHPullRequestsResponseReviewCommentsUrl = @"review_comments_url";
NSString *const kGHPullRequestsResponseAssignee = @"assignee";
NSString *const kGHPullRequestsResponseCommentsUrl = @"comments_url";
NSString *const kGHPullRequestsResponsePatchUrl = @"patch_url";
NSString *const kGHPullRequestsResponseMergedAt = @"merged_at";
NSString *const kGHPullRequestsResponseState = @"state";
NSString *const kGHPullRequestsResponseBody = @"body";
NSString *const kGHPullRequestsResponseId = @"id";
NSString *const kGHPullRequestsResponseNumber = @"number";
NSString *const kGHPullRequestsResponseIssueUrl = @"issue_url";
NSString *const kGHPullRequestsResponseUser = @"user";
NSString *const kGHPullRequestsResponseClosedAt = @"closed_at";
NSString *const kGHPullRequestsResponseHead = @"head";
NSString *const kGHPullRequestsResponseStatusesUrl = @"statuses_url";
NSString *const kGHPullRequestsResponseCreatedAt = @"created_at";
NSString *const kGHPullRequestsResponseLinks = @"_links";
NSString *const kGHPullRequestsResponseDiffUrl = @"diff_url";
NSString *const kGHPullRequestsResponseHtmlUrl = @"html_url";


@interface GHPullRequestsResponse ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GHPullRequestsResponse

//@synthesize milestone = _milestone;
@synthesize locked = _locked;
@synthesize title = _title;
@synthesize url = _url;
@synthesize commitsUrl = _commitsUrl;
@synthesize mergeCommitSha = _mergeCommitSha;
@synthesize reviewCommentUrl = _reviewCommentUrl;
@synthesize updatedAt = _updatedAt;
@synthesize base = _base;
@synthesize reviewCommentsUrl = _reviewCommentsUrl;
//@synthesize assignee = _assignee;
@synthesize commentsUrl = _commentsUrl;
@synthesize patchUrl = _patchUrl;
//@synthesize mergedAt = _mergedAt;
@synthesize state = _state;
@synthesize body = _body;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize number = _number;
@synthesize issueUrl = _issueUrl;
@synthesize user = _user;
//@synthesize closedAt = _closedAt;
@synthesize head = _head;
@synthesize statusesUrl = _statusesUrl;
@synthesize createdAt = _createdAt;
@synthesize links = _links;
@synthesize diffUrl = _diffUrl;
@synthesize htmlUrl = _htmlUrl;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
//            self.milestone = [self objectOrNilForKey:kGHPullRequestsResponseMilestone fromDictionary:dict];
            self.locked = [[self objectOrNilForKey:kGHPullRequestsResponseLocked fromDictionary:dict] boolValue];
            self.title = [self objectOrNilForKey:kGHPullRequestsResponseTitle fromDictionary:dict];
            self.url = [self objectOrNilForKey:kGHPullRequestsResponseUrl fromDictionary:dict];
            self.commitsUrl = [self objectOrNilForKey:kGHPullRequestsResponseCommitsUrl fromDictionary:dict];
            self.mergeCommitSha = [self objectOrNilForKey:kGHPullRequestsResponseMergeCommitSha fromDictionary:dict];
            self.reviewCommentUrl = [self objectOrNilForKey:kGHPullRequestsResponseReviewCommentUrl fromDictionary:dict];
            self.updatedAt = [self objectOrNilForKey:kGHPullRequestsResponseUpdatedAt fromDictionary:dict];
            self.base = [GHBase modelObjectWithDictionary:[dict objectForKey:kGHPullRequestsResponseBase]];
            self.reviewCommentsUrl = [self objectOrNilForKey:kGHPullRequestsResponseReviewCommentsUrl fromDictionary:dict];
//            self.assignee = [self objectOrNilForKey:kGHPullRequestsResponseAssignee fromDictionary:dict];
            self.commentsUrl = [self objectOrNilForKey:kGHPullRequestsResponseCommentsUrl fromDictionary:dict];
            self.patchUrl = [self objectOrNilForKey:kGHPullRequestsResponsePatchUrl fromDictionary:dict];
//            self.mergedAt = [self objectOrNilForKey:kGHPullRequestsResponseMergedAt fromDictionary:dict];
            self.state = [self objectOrNilForKey:kGHPullRequestsResponseState fromDictionary:dict];
            self.body = [self objectOrNilForKey:kGHPullRequestsResponseBody fromDictionary:dict];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kGHPullRequestsResponseId fromDictionary:dict] doubleValue];
            self.number = [[self objectOrNilForKey:kGHPullRequestsResponseNumber fromDictionary:dict] doubleValue];
            self.issueUrl = [self objectOrNilForKey:kGHPullRequestsResponseIssueUrl fromDictionary:dict];
            self.user = [GHUser modelObjectWithDictionary:[dict objectForKey:kGHPullRequestsResponseUser]];
//            self.closedAt = [self objectOrNilForKey:kGHPullRequestsResponseClosedAt fromDictionary:dict];
            self.head = [GHHead modelObjectWithDictionary:[dict objectForKey:kGHPullRequestsResponseHead]];
            self.statusesUrl = [self objectOrNilForKey:kGHPullRequestsResponseStatusesUrl fromDictionary:dict];
            self.createdAt = [self objectOrNilForKey:kGHPullRequestsResponseCreatedAt fromDictionary:dict];
            self.links = [GHLinks modelObjectWithDictionary:[dict objectForKey:kGHPullRequestsResponseLinks]];
            self.diffUrl = [self objectOrNilForKey:kGHPullRequestsResponseDiffUrl fromDictionary:dict];
            self.htmlUrl = [self objectOrNilForKey:kGHPullRequestsResponseHtmlUrl fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
//    [mutableDict setValue:self.milestone forKey:kGHPullRequestsResponseMilestone];
    [mutableDict setValue:[NSNumber numberWithBool:self.locked] forKey:kGHPullRequestsResponseLocked];
    [mutableDict setValue:self.title forKey:kGHPullRequestsResponseTitle];
    [mutableDict setValue:self.url forKey:kGHPullRequestsResponseUrl];
    [mutableDict setValue:self.commitsUrl forKey:kGHPullRequestsResponseCommitsUrl];
    [mutableDict setValue:self.mergeCommitSha forKey:kGHPullRequestsResponseMergeCommitSha];
    [mutableDict setValue:self.reviewCommentUrl forKey:kGHPullRequestsResponseReviewCommentUrl];
    [mutableDict setValue:self.updatedAt forKey:kGHPullRequestsResponseUpdatedAt];
    [mutableDict setValue:[self.base dictionaryRepresentation] forKey:kGHPullRequestsResponseBase];
    [mutableDict setValue:self.reviewCommentsUrl forKey:kGHPullRequestsResponseReviewCommentsUrl];
//    [mutableDict setValue:self.assignee forKey:kGHPullRequestsResponseAssignee];
    [mutableDict setValue:self.commentsUrl forKey:kGHPullRequestsResponseCommentsUrl];
    [mutableDict setValue:self.patchUrl forKey:kGHPullRequestsResponsePatchUrl];
//    [mutableDict setValue:self.mergedAt forKey:kGHPullRequestsResponseMergedAt];
    [mutableDict setValue:self.state forKey:kGHPullRequestsResponseState];
    [mutableDict setValue:self.body forKey:kGHPullRequestsResponseBody];
    [mutableDict setValue:[NSNumber numberWithDouble:self.internalBaseClassIdentifier] forKey:kGHPullRequestsResponseId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.number] forKey:kGHPullRequestsResponseNumber];
    [mutableDict setValue:self.issueUrl forKey:kGHPullRequestsResponseIssueUrl];
    [mutableDict setValue:[self.user dictionaryRepresentation] forKey:kGHPullRequestsResponseUser];
//    [mutableDict setValue:self.closedAt forKey:kGHPullRequestsResponseClosedAt];
    [mutableDict setValue:[self.head dictionaryRepresentation] forKey:kGHPullRequestsResponseHead];
    [mutableDict setValue:self.statusesUrl forKey:kGHPullRequestsResponseStatusesUrl];
    [mutableDict setValue:self.createdAt forKey:kGHPullRequestsResponseCreatedAt];
    [mutableDict setValue:[self.links dictionaryRepresentation] forKey:kGHPullRequestsResponseLinks];
    [mutableDict setValue:self.diffUrl forKey:kGHPullRequestsResponseDiffUrl];
    [mutableDict setValue:self.htmlUrl forKey:kGHPullRequestsResponseHtmlUrl];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
