//
//  GHHead.m
//
//  Created by Marcelo  on 09/04/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GHHead.h"
#import "GHUser.h"


NSString *const kGHHeadRef = @"ref";
NSString *const kGHHeadLabel = @"label";
NSString *const kGHHeadSha = @"sha";
NSString *const kGHHeadRepo = @"repo";
NSString *const kGHHeadUser = @"user";


@interface GHHead ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GHHead

@synthesize ref = _ref;
@synthesize label = _label;
@synthesize sha = _sha;
@synthesize repo = _repo;
@synthesize user = _user;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.ref = [self objectOrNilForKey:kGHHeadRef fromDictionary:dict];
            self.label = [self objectOrNilForKey:kGHHeadLabel fromDictionary:dict];
            self.sha = [self objectOrNilForKey:kGHHeadSha fromDictionary:dict];
            self.repo = [GHItems modelObjectWithDictionary:[dict objectForKey:kGHHeadRepo]];
            self.user = [GHUser modelObjectWithDictionary:[dict objectForKey:kGHHeadUser]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.ref forKey:kGHHeadRef];
    [mutableDict setValue:self.label forKey:kGHHeadLabel];
    [mutableDict setValue:self.sha forKey:kGHHeadSha];
    [mutableDict setValue:[self.repo dictionaryRepresentation] forKey:kGHHeadRepo];
    [mutableDict setValue:[self.user dictionaryRepresentation] forKey:kGHHeadUser];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
