//
//  GHPullRequestsResponse.h
//
//  Created by Marcelo  on 09/04/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GHBase, GHUser, GHHead, GHLinks;

@interface GHPullRequestsResponse : NSObject

//@property (nonatomic, assign) id milestone;
@property (nonatomic, assign) BOOL locked;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *commitsUrl;
@property (nonatomic, strong) NSString *mergeCommitSha;
@property (nonatomic, strong) NSString *reviewCommentUrl;
@property (nonatomic, strong) NSString *updatedAt;
@property (nonatomic, strong) GHBase *base;
@property (nonatomic, strong) NSString *reviewCommentsUrl;
//@property (nonatomic, assign) id assignee;
@property (nonatomic, strong) NSString *commentsUrl;
@property (nonatomic, strong) NSString *patchUrl;
//@property (nonatomic, assign) id mergedAt;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *body;
@property (nonatomic, assign) double internalBaseClassIdentifier;
@property (nonatomic, assign) double number;
@property (nonatomic, strong) NSString *issueUrl;
@property (nonatomic, strong) GHUser *user;
//@property (nonatomic, assign) id closedAt;
@property (nonatomic, strong) GHHead *head;
@property (nonatomic, strong) NSString *statusesUrl;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) GHLinks *links;
@property (nonatomic, strong) NSString *diffUrl;
@property (nonatomic, strong) NSString *htmlUrl;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
