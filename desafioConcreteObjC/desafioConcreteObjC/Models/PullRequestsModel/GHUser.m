//
//  GHUser.m
//
//  Created by Marcelo  on 09/04/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GHUser.h"


NSString *const kGHUserId = @"id";
NSString *const kGHUserOrganizationsUrl = @"organizations_url";
NSString *const kGHUserReceivedEventsUrl = @"received_events_url";
NSString *const kGHUserFollowingUrl = @"following_url";
NSString *const kGHUserLogin = @"login";
NSString *const kGHUserSubscriptionsUrl = @"subscriptions_url";
NSString *const kGHUserAvatarUrl = @"avatar_url";
NSString *const kGHUserUrl = @"url";
NSString *const kGHUserType = @"type";
NSString *const kGHUserReposUrl = @"repos_url";
NSString *const kGHUserHtmlUrl = @"html_url";
NSString *const kGHUserEventsUrl = @"events_url";
NSString *const kGHUserSiteAdmin = @"site_admin";
NSString *const kGHUserStarredUrl = @"starred_url";
NSString *const kGHUserGistsUrl = @"gists_url";
NSString *const kGHUserGravatarId = @"gravatar_id";
NSString *const kGHUserFollowersUrl = @"followers_url";


@interface GHUser ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GHUser

@synthesize userIdentifier = _userIdentifier;
@synthesize organizationsUrl = _organizationsUrl;
@synthesize receivedEventsUrl = _receivedEventsUrl;
@synthesize followingUrl = _followingUrl;
@synthesize login = _login;
@synthesize subscriptionsUrl = _subscriptionsUrl;
@synthesize avatarUrl = _avatarUrl;
@synthesize url = _url;
@synthesize type = _type;
@synthesize reposUrl = _reposUrl;
@synthesize htmlUrl = _htmlUrl;
@synthesize eventsUrl = _eventsUrl;
@synthesize siteAdmin = _siteAdmin;
@synthesize starredUrl = _starredUrl;
@synthesize gistsUrl = _gistsUrl;
@synthesize gravatarId = _gravatarId;
@synthesize followersUrl = _followersUrl;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.userIdentifier = [[self objectOrNilForKey:kGHUserId fromDictionary:dict] doubleValue];
            self.organizationsUrl = [self objectOrNilForKey:kGHUserOrganizationsUrl fromDictionary:dict];
            self.receivedEventsUrl = [self objectOrNilForKey:kGHUserReceivedEventsUrl fromDictionary:dict];
            self.followingUrl = [self objectOrNilForKey:kGHUserFollowingUrl fromDictionary:dict];
            self.login = [self objectOrNilForKey:kGHUserLogin fromDictionary:dict];
            self.subscriptionsUrl = [self objectOrNilForKey:kGHUserSubscriptionsUrl fromDictionary:dict];
            self.avatarUrl = [self objectOrNilForKey:kGHUserAvatarUrl fromDictionary:dict];
            self.url = [self objectOrNilForKey:kGHUserUrl fromDictionary:dict];
            self.type = [self objectOrNilForKey:kGHUserType fromDictionary:dict];
            self.reposUrl = [self objectOrNilForKey:kGHUserReposUrl fromDictionary:dict];
            self.htmlUrl = [self objectOrNilForKey:kGHUserHtmlUrl fromDictionary:dict];
            self.eventsUrl = [self objectOrNilForKey:kGHUserEventsUrl fromDictionary:dict];
            self.siteAdmin = [[self objectOrNilForKey:kGHUserSiteAdmin fromDictionary:dict] boolValue];
            self.starredUrl = [self objectOrNilForKey:kGHUserStarredUrl fromDictionary:dict];
            self.gistsUrl = [self objectOrNilForKey:kGHUserGistsUrl fromDictionary:dict];
            self.gravatarId = [self objectOrNilForKey:kGHUserGravatarId fromDictionary:dict];
            self.followersUrl = [self objectOrNilForKey:kGHUserFollowersUrl fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userIdentifier] forKey:kGHUserId];
    [mutableDict setValue:self.organizationsUrl forKey:kGHUserOrganizationsUrl];
    [mutableDict setValue:self.receivedEventsUrl forKey:kGHUserReceivedEventsUrl];
    [mutableDict setValue:self.followingUrl forKey:kGHUserFollowingUrl];
    [mutableDict setValue:self.login forKey:kGHUserLogin];
    [mutableDict setValue:self.subscriptionsUrl forKey:kGHUserSubscriptionsUrl];
    [mutableDict setValue:self.avatarUrl forKey:kGHUserAvatarUrl];
    [mutableDict setValue:self.url forKey:kGHUserUrl];
    [mutableDict setValue:self.type forKey:kGHUserType];
    [mutableDict setValue:self.reposUrl forKey:kGHUserReposUrl];
    [mutableDict setValue:self.htmlUrl forKey:kGHUserHtmlUrl];
    [mutableDict setValue:self.eventsUrl forKey:kGHUserEventsUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.siteAdmin] forKey:kGHUserSiteAdmin];
    [mutableDict setValue:self.starredUrl forKey:kGHUserStarredUrl];
    [mutableDict setValue:self.gistsUrl forKey:kGHUserGistsUrl];
    [mutableDict setValue:self.gravatarId forKey:kGHUserGravatarId];
    [mutableDict setValue:self.followersUrl forKey:kGHUserFollowersUrl];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
