//
//  GHRepo.m
//
//  Created by Marcelo  on 09/04/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "GHRepo.h"
#import "GHOwner.h"


NSString *const kGHRepoKeysUrl = @"keys_url";
NSString *const kGHRepoStatusesUrl = @"statuses_url";
NSString *const kGHRepoIssuesUrl = @"issues_url";
NSString *const kGHRepoLanguage = @"language";
NSString *const kGHRepoIssueEventsUrl = @"issue_events_url";
NSString *const kGHRepoId = @"id";
NSString *const kGHRepoOwner = @"owner";
NSString *const kGHRepoEventsUrl = @"events_url";
NSString *const kGHRepoSubscriptionUrl = @"subscription_url";
NSString *const kGHRepoWatchers = @"watchers";
NSString *const kGHRepoGitCommitsUrl = @"git_commits_url";
NSString *const kGHRepoSubscribersUrl = @"subscribers_url";
NSString *const kGHRepoCloneUrl = @"clone_url";
NSString *const kGHRepoHasWiki = @"has_wiki";
NSString *const kGHRepoPullsUrl = @"pulls_url";
NSString *const kGHRepoUrl = @"url";
NSString *const kGHRepoFork = @"fork";
NSString *const kGHRepoNotificationsUrl = @"notifications_url";
NSString *const kGHRepoDescription = @"description";
NSString *const kGHRepoCollaboratorsUrl = @"collaborators_url";
NSString *const kGHRepoDeploymentsUrl = @"deployments_url";
NSString *const kGHRepoLanguagesUrl = @"languages_url";
NSString *const kGHRepoHasIssues = @"has_issues";
NSString *const kGHRepoCommentsUrl = @"comments_url";
NSString *const kGHRepoPrivate = @"private";
NSString *const kGHRepoSize = @"size";
NSString *const kGHRepoGitTagsUrl = @"git_tags_url";
NSString *const kGHRepoUpdatedAt = @"updated_at";
NSString *const kGHRepoSshUrl = @"ssh_url";
NSString *const kGHRepoName = @"name";
NSString *const kGHRepoArchiveUrl = @"archive_url";
NSString *const kGHRepoOpenIssuesCount = @"open_issues_count";
NSString *const kGHRepoMilestonesUrl = @"milestones_url";
NSString *const kGHRepoBlobsUrl = @"blobs_url";
NSString *const kGHRepoContributorsUrl = @"contributors_url";
NSString *const kGHRepoContentsUrl = @"contents_url";
NSString *const kGHRepoForksCount = @"forks_count";
NSString *const kGHRepoTreesUrl = @"trees_url";
NSString *const kGHRepoMirrorUrl = @"mirror_url";
NSString *const kGHRepoHasDownloads = @"has_downloads";
NSString *const kGHRepoCreatedAt = @"created_at";
NSString *const kGHRepoForksUrl = @"forks_url";
NSString *const kGHRepoSvnUrl = @"svn_url";
NSString *const kGHRepoCommitsUrl = @"commits_url";
NSString *const kGHRepoHomepage = @"homepage";
NSString *const kGHRepoTeamsUrl = @"teams_url";
NSString *const kGHRepoBranchesUrl = @"branches_url";
NSString *const kGHRepoIssueCommentUrl = @"issue_comment_url";
NSString *const kGHRepoMergesUrl = @"merges_url";
NSString *const kGHRepoGitRefsUrl = @"git_refs_url";
NSString *const kGHRepoGitUrl = @"git_url";
NSString *const kGHRepoForks = @"forks";
NSString *const kGHRepoOpenIssues = @"open_issues";
NSString *const kGHRepoHooksUrl = @"hooks_url";
NSString *const kGHRepoHtmlUrl = @"html_url";
NSString *const kGHRepoStargazersUrl = @"stargazers_url";
NSString *const kGHRepoHasPages = @"has_pages";
NSString *const kGHRepoAssigneesUrl = @"assignees_url";
NSString *const kGHRepoCompareUrl = @"compare_url";
NSString *const kGHRepoFullName = @"full_name";
NSString *const kGHRepoTagsUrl = @"tags_url";
NSString *const kGHRepoReleasesUrl = @"releases_url";
NSString *const kGHRepoPushedAt = @"pushed_at";
NSString *const kGHRepoLabelsUrl = @"labels_url";
NSString *const kGHRepoDownloadsUrl = @"downloads_url";
NSString *const kGHRepoDefaultBranch = @"default_branch";
NSString *const kGHRepoStargazersCount = @"stargazers_count";
NSString *const kGHRepoWatchersCount = @"watchers_count";


@interface GHRepo ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GHRepo

@synthesize keysUrl = _keysUrl;
@synthesize statusesUrl = _statusesUrl;
@synthesize issuesUrl = _issuesUrl;
@synthesize language = _language;
@synthesize issueEventsUrl = _issueEventsUrl;
@synthesize repoIdentifier = _repoIdentifier;
@synthesize owner = _owner;
@synthesize eventsUrl = _eventsUrl;
@synthesize subscriptionUrl = _subscriptionUrl;
@synthesize watchers = _watchers;
@synthesize gitCommitsUrl = _gitCommitsUrl;
@synthesize subscribersUrl = _subscribersUrl;
@synthesize cloneUrl = _cloneUrl;
@synthesize hasWiki = _hasWiki;
@synthesize pullsUrl = _pullsUrl;
@synthesize url = _url;
@synthesize fork = _fork;
@synthesize notificationsUrl = _notificationsUrl;
@synthesize repoDescription = _repoDescription;
@synthesize collaboratorsUrl = _collaboratorsUrl;
@synthesize deploymentsUrl = _deploymentsUrl;
@synthesize languagesUrl = _languagesUrl;
@synthesize hasIssues = _hasIssues;
@synthesize commentsUrl = _commentsUrl;
@synthesize privateProperty = _privateProperty;
@synthesize size = _size;
@synthesize gitTagsUrl = _gitTagsUrl;
@synthesize updatedAt = _updatedAt;
@synthesize sshUrl = _sshUrl;
@synthesize name = _name;
@synthesize archiveUrl = _archiveUrl;
@synthesize openIssuesCount = _openIssuesCount;
@synthesize milestonesUrl = _milestonesUrl;
@synthesize blobsUrl = _blobsUrl;
@synthesize contributorsUrl = _contributorsUrl;
@synthesize contentsUrl = _contentsUrl;
@synthesize forksCount = _forksCount;
@synthesize treesUrl = _treesUrl;
//@synthesize mirrorUrl = _mirrorUrl;
@synthesize hasDownloads = _hasDownloads;
@synthesize createdAt = _createdAt;
@synthesize forksUrl = _forksUrl;
@synthesize svnUrl = _svnUrl;
@synthesize commitsUrl = _commitsUrl;
@synthesize homepage = _homepage;
@synthesize teamsUrl = _teamsUrl;
@synthesize branchesUrl = _branchesUrl;
@synthesize issueCommentUrl = _issueCommentUrl;
@synthesize mergesUrl = _mergesUrl;
@synthesize gitRefsUrl = _gitRefsUrl;
@synthesize gitUrl = _gitUrl;
@synthesize forks = _forks;
@synthesize openIssues = _openIssues;
@synthesize hooksUrl = _hooksUrl;
@synthesize htmlUrl = _htmlUrl;
@synthesize stargazersUrl = _stargazersUrl;
@synthesize hasPages = _hasPages;
@synthesize assigneesUrl = _assigneesUrl;
@synthesize compareUrl = _compareUrl;
@synthesize fullName = _fullName;
@synthesize tagsUrl = _tagsUrl;
@synthesize releasesUrl = _releasesUrl;
@synthesize pushedAt = _pushedAt;
@synthesize labelsUrl = _labelsUrl;
@synthesize downloadsUrl = _downloadsUrl;
@synthesize defaultBranch = _defaultBranch;
@synthesize stargazersCount = _stargazersCount;
@synthesize watchersCount = _watchersCount;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.keysUrl = [self objectOrNilForKey:kGHRepoKeysUrl fromDictionary:dict];
            self.statusesUrl = [self objectOrNilForKey:kGHRepoStatusesUrl fromDictionary:dict];
            self.issuesUrl = [self objectOrNilForKey:kGHRepoIssuesUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kGHRepoLanguage fromDictionary:dict];
            self.issueEventsUrl = [self objectOrNilForKey:kGHRepoIssueEventsUrl fromDictionary:dict];
            self.repoIdentifier = [[self objectOrNilForKey:kGHRepoId fromDictionary:dict] doubleValue];
            self.owner = [GHOwner modelObjectWithDictionary:[dict objectForKey:kGHRepoOwner]];
            self.eventsUrl = [self objectOrNilForKey:kGHRepoEventsUrl fromDictionary:dict];
            self.subscriptionUrl = [self objectOrNilForKey:kGHRepoSubscriptionUrl fromDictionary:dict];
            self.watchers = [[self objectOrNilForKey:kGHRepoWatchers fromDictionary:dict] doubleValue];
            self.gitCommitsUrl = [self objectOrNilForKey:kGHRepoGitCommitsUrl fromDictionary:dict];
            self.subscribersUrl = [self objectOrNilForKey:kGHRepoSubscribersUrl fromDictionary:dict];
            self.cloneUrl = [self objectOrNilForKey:kGHRepoCloneUrl fromDictionary:dict];
            self.hasWiki = [[self objectOrNilForKey:kGHRepoHasWiki fromDictionary:dict] boolValue];
            self.pullsUrl = [self objectOrNilForKey:kGHRepoPullsUrl fromDictionary:dict];
            self.url = [self objectOrNilForKey:kGHRepoUrl fromDictionary:dict];
            self.fork = [[self objectOrNilForKey:kGHRepoFork fromDictionary:dict] boolValue];
            self.notificationsUrl = [self objectOrNilForKey:kGHRepoNotificationsUrl fromDictionary:dict];
            self.repoDescription = [self objectOrNilForKey:kGHRepoDescription fromDictionary:dict];
            self.collaboratorsUrl = [self objectOrNilForKey:kGHRepoCollaboratorsUrl fromDictionary:dict];
            self.deploymentsUrl = [self objectOrNilForKey:kGHRepoDeploymentsUrl fromDictionary:dict];
            self.languagesUrl = [self objectOrNilForKey:kGHRepoLanguagesUrl fromDictionary:dict];
            self.hasIssues = [[self objectOrNilForKey:kGHRepoHasIssues fromDictionary:dict] boolValue];
            self.commentsUrl = [self objectOrNilForKey:kGHRepoCommentsUrl fromDictionary:dict];
            self.privateProperty = [[self objectOrNilForKey:kGHRepoPrivate fromDictionary:dict] boolValue];
            self.size = [[self objectOrNilForKey:kGHRepoSize fromDictionary:dict] doubleValue];
            self.gitTagsUrl = [self objectOrNilForKey:kGHRepoGitTagsUrl fromDictionary:dict];
            self.updatedAt = [self objectOrNilForKey:kGHRepoUpdatedAt fromDictionary:dict];
            self.sshUrl = [self objectOrNilForKey:kGHRepoSshUrl fromDictionary:dict];
            self.name = [self objectOrNilForKey:kGHRepoName fromDictionary:dict];
            self.archiveUrl = [self objectOrNilForKey:kGHRepoArchiveUrl fromDictionary:dict];
            self.openIssuesCount = [[self objectOrNilForKey:kGHRepoOpenIssuesCount fromDictionary:dict] doubleValue];
            self.milestonesUrl = [self objectOrNilForKey:kGHRepoMilestonesUrl fromDictionary:dict];
            self.blobsUrl = [self objectOrNilForKey:kGHRepoBlobsUrl fromDictionary:dict];
            self.contributorsUrl = [self objectOrNilForKey:kGHRepoContributorsUrl fromDictionary:dict];
            self.contentsUrl = [self objectOrNilForKey:kGHRepoContentsUrl fromDictionary:dict];
            self.forksCount = [[self objectOrNilForKey:kGHRepoForksCount fromDictionary:dict] doubleValue];
            self.treesUrl = [self objectOrNilForKey:kGHRepoTreesUrl fromDictionary:dict];
//            self.mirrorUrl = [self objectOrNilForKey:kGHRepoMirrorUrl fromDictionary:dict];
            self.hasDownloads = [[self objectOrNilForKey:kGHRepoHasDownloads fromDictionary:dict] boolValue];
            self.createdAt = [self objectOrNilForKey:kGHRepoCreatedAt fromDictionary:dict];
            self.forksUrl = [self objectOrNilForKey:kGHRepoForksUrl fromDictionary:dict];
            self.svnUrl = [self objectOrNilForKey:kGHRepoSvnUrl fromDictionary:dict];
            self.commitsUrl = [self objectOrNilForKey:kGHRepoCommitsUrl fromDictionary:dict];
            self.homepage = [self objectOrNilForKey:kGHRepoHomepage fromDictionary:dict];
            self.teamsUrl = [self objectOrNilForKey:kGHRepoTeamsUrl fromDictionary:dict];
            self.branchesUrl = [self objectOrNilForKey:kGHRepoBranchesUrl fromDictionary:dict];
            self.issueCommentUrl = [self objectOrNilForKey:kGHRepoIssueCommentUrl fromDictionary:dict];
            self.mergesUrl = [self objectOrNilForKey:kGHRepoMergesUrl fromDictionary:dict];
            self.gitRefsUrl = [self objectOrNilForKey:kGHRepoGitRefsUrl fromDictionary:dict];
            self.gitUrl = [self objectOrNilForKey:kGHRepoGitUrl fromDictionary:dict];
            self.forks = [[self objectOrNilForKey:kGHRepoForks fromDictionary:dict] doubleValue];
            self.openIssues = [[self objectOrNilForKey:kGHRepoOpenIssues fromDictionary:dict] doubleValue];
            self.hooksUrl = [self objectOrNilForKey:kGHRepoHooksUrl fromDictionary:dict];
            self.htmlUrl = [self objectOrNilForKey:kGHRepoHtmlUrl fromDictionary:dict];
            self.stargazersUrl = [self objectOrNilForKey:kGHRepoStargazersUrl fromDictionary:dict];
            self.hasPages = [[self objectOrNilForKey:kGHRepoHasPages fromDictionary:dict] boolValue];
            self.assigneesUrl = [self objectOrNilForKey:kGHRepoAssigneesUrl fromDictionary:dict];
            self.compareUrl = [self objectOrNilForKey:kGHRepoCompareUrl fromDictionary:dict];
            self.fullName = [self objectOrNilForKey:kGHRepoFullName fromDictionary:dict];
            self.tagsUrl = [self objectOrNilForKey:kGHRepoTagsUrl fromDictionary:dict];
            self.releasesUrl = [self objectOrNilForKey:kGHRepoReleasesUrl fromDictionary:dict];
            self.pushedAt = [self objectOrNilForKey:kGHRepoPushedAt fromDictionary:dict];
            self.labelsUrl = [self objectOrNilForKey:kGHRepoLabelsUrl fromDictionary:dict];
            self.downloadsUrl = [self objectOrNilForKey:kGHRepoDownloadsUrl fromDictionary:dict];
            self.defaultBranch = [self objectOrNilForKey:kGHRepoDefaultBranch fromDictionary:dict];
            self.stargazersCount = [[self objectOrNilForKey:kGHRepoStargazersCount fromDictionary:dict] doubleValue];
            self.watchersCount = [[self objectOrNilForKey:kGHRepoWatchersCount fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.keysUrl forKey:kGHRepoKeysUrl];
    [mutableDict setValue:self.statusesUrl forKey:kGHRepoStatusesUrl];
    [mutableDict setValue:self.issuesUrl forKey:kGHRepoIssuesUrl];
    [mutableDict setValue:self.language forKey:kGHRepoLanguage];
    [mutableDict setValue:self.issueEventsUrl forKey:kGHRepoIssueEventsUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.repoIdentifier] forKey:kGHRepoId];
    [mutableDict setValue:[self.owner dictionaryRepresentation] forKey:kGHRepoOwner];
    [mutableDict setValue:self.eventsUrl forKey:kGHRepoEventsUrl];
    [mutableDict setValue:self.subscriptionUrl forKey:kGHRepoSubscriptionUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.watchers] forKey:kGHRepoWatchers];
    [mutableDict setValue:self.gitCommitsUrl forKey:kGHRepoGitCommitsUrl];
    [mutableDict setValue:self.subscribersUrl forKey:kGHRepoSubscribersUrl];
    [mutableDict setValue:self.cloneUrl forKey:kGHRepoCloneUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.hasWiki] forKey:kGHRepoHasWiki];
    [mutableDict setValue:self.pullsUrl forKey:kGHRepoPullsUrl];
    [mutableDict setValue:self.url forKey:kGHRepoUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.fork] forKey:kGHRepoFork];
    [mutableDict setValue:self.notificationsUrl forKey:kGHRepoNotificationsUrl];
    [mutableDict setValue:self.repoDescription forKey:kGHRepoDescription];
    [mutableDict setValue:self.collaboratorsUrl forKey:kGHRepoCollaboratorsUrl];
    [mutableDict setValue:self.deploymentsUrl forKey:kGHRepoDeploymentsUrl];
    [mutableDict setValue:self.languagesUrl forKey:kGHRepoLanguagesUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.hasIssues] forKey:kGHRepoHasIssues];
    [mutableDict setValue:self.commentsUrl forKey:kGHRepoCommentsUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.privateProperty] forKey:kGHRepoPrivate];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kGHRepoSize];
    [mutableDict setValue:self.gitTagsUrl forKey:kGHRepoGitTagsUrl];
    [mutableDict setValue:self.updatedAt forKey:kGHRepoUpdatedAt];
    [mutableDict setValue:self.sshUrl forKey:kGHRepoSshUrl];
    [mutableDict setValue:self.name forKey:kGHRepoName];
    [mutableDict setValue:self.archiveUrl forKey:kGHRepoArchiveUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.openIssuesCount] forKey:kGHRepoOpenIssuesCount];
    [mutableDict setValue:self.milestonesUrl forKey:kGHRepoMilestonesUrl];
    [mutableDict setValue:self.blobsUrl forKey:kGHRepoBlobsUrl];
    [mutableDict setValue:self.contributorsUrl forKey:kGHRepoContributorsUrl];
    [mutableDict setValue:self.contentsUrl forKey:kGHRepoContentsUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.forksCount] forKey:kGHRepoForksCount];
    [mutableDict setValue:self.treesUrl forKey:kGHRepoTreesUrl];
//    [mutableDict setValue:self.mirrorUrl forKey:kGHRepoMirrorUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.hasDownloads] forKey:kGHRepoHasDownloads];
    [mutableDict setValue:self.createdAt forKey:kGHRepoCreatedAt];
    [mutableDict setValue:self.forksUrl forKey:kGHRepoForksUrl];
    [mutableDict setValue:self.svnUrl forKey:kGHRepoSvnUrl];
    [mutableDict setValue:self.commitsUrl forKey:kGHRepoCommitsUrl];
    [mutableDict setValue:self.homepage forKey:kGHRepoHomepage];
    [mutableDict setValue:self.teamsUrl forKey:kGHRepoTeamsUrl];
    [mutableDict setValue:self.branchesUrl forKey:kGHRepoBranchesUrl];
    [mutableDict setValue:self.issueCommentUrl forKey:kGHRepoIssueCommentUrl];
    [mutableDict setValue:self.mergesUrl forKey:kGHRepoMergesUrl];
    [mutableDict setValue:self.gitRefsUrl forKey:kGHRepoGitRefsUrl];
    [mutableDict setValue:self.gitUrl forKey:kGHRepoGitUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.forks] forKey:kGHRepoForks];
    [mutableDict setValue:[NSNumber numberWithDouble:self.openIssues] forKey:kGHRepoOpenIssues];
    [mutableDict setValue:self.hooksUrl forKey:kGHRepoHooksUrl];
    [mutableDict setValue:self.htmlUrl forKey:kGHRepoHtmlUrl];
    [mutableDict setValue:self.stargazersUrl forKey:kGHRepoStargazersUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.hasPages] forKey:kGHRepoHasPages];
    [mutableDict setValue:self.assigneesUrl forKey:kGHRepoAssigneesUrl];
    [mutableDict setValue:self.compareUrl forKey:kGHRepoCompareUrl];
    [mutableDict setValue:self.fullName forKey:kGHRepoFullName];
    [mutableDict setValue:self.tagsUrl forKey:kGHRepoTagsUrl];
    [mutableDict setValue:self.releasesUrl forKey:kGHRepoReleasesUrl];
    [mutableDict setValue:self.pushedAt forKey:kGHRepoPushedAt];
    [mutableDict setValue:self.labelsUrl forKey:kGHRepoLabelsUrl];
    [mutableDict setValue:self.downloadsUrl forKey:kGHRepoDownloadsUrl];
    [mutableDict setValue:self.defaultBranch forKey:kGHRepoDefaultBranch];
    [mutableDict setValue:[NSNumber numberWithDouble:self.stargazersCount] forKey:kGHRepoStargazersCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.watchersCount] forKey:kGHRepoWatchersCount];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
