//
//  BaseManager.h
//  XMobPOS
//
//  Created by Rafael Baraldi on 25/09/15.
//  Copyright (c) 2015 resource. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepositoriesDataModels.h"

@interface BaseManager : NSObject

+ (void)getRepositories:(int)page
               onSucces:(void (^)(GHRepositoriesResponse* respObj))successFuction
                onError:(void (^)(id err))errorFuction;


+ (void)getPullRequests:(NSString*)owner repo:(NSString*)repo
              onSuccess:(void (^)(NSArray* respObj))successFuction
                onError:(void (^)(id err))errorFuction;


@end
