//
//  BaseManager.m
//  XMobPOS
//
//  Created by Rafael Baraldi on 25/09/15.
//  Copyright (c) 2015 resource. All rights reserved.
//

#import "BaseManager.h"
#import <AFNetworking/AFNetworking.h>
#import "PullRequestsDataModels.h"

@implementation BaseManager

+ (void)getRepositories:(int)page
               onSucces:(void (^)(GHRepositoriesResponse* respObj))successFuction
                onError:(void (^)(id err))errorFuction
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSString* urlString = [NSString stringWithFormat:@"https://api.github.com/search/repositories?q=language:Java&sort=stars&page=%d", page];
    
    NSURL *URL = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request
                                                completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
            
            errorFuction(error);
            
        } else {
            NSLog(@"%@ %@", response, responseObject);
            
            successFuction([[GHRepositoriesResponse alloc] initWithDictionary:responseObject]);
        }
    }];
    [dataTask resume];
}

+ (void)getPullRequests:(NSString*)owner repo:(NSString*)repo
              onSuccess:(void (^)(NSArray* respObj))successFuction
                onError:(void (^)(id err))errorFuction
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSString* urlString = [NSString stringWithFormat:@"https://api.github.com/repos/%@/%@/pulls", owner, repo];
    
    NSURL *URL = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request
                                                completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
            
            errorFuction(error);
            
        } else {
            NSLog(@"%@ %@", response, responseObject);
            
            successFuction((NSArray*)responseObject);
        }
    }];
    [dataTask resume];
}

@end
