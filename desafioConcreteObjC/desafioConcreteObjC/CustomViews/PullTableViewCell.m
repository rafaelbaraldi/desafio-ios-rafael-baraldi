//
//  PullTableViewCell.m
//  desafioConcreteObjC
//
//  Created by Rafael Baraldi on 10/04/16.
//  Copyright © 2016 Rafael BAraldi. All rights reserved.
//

#import "PullTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation PullTableViewCell

-(void)fillCellWithItem:(GHPullRequestsResponse*)item{
    
    self.lblName.text = item.title;
    self.lblDesc.text = item.body;
    self.lblOwner.text = item.user.login;
    
    [self.imgOwner sd_setImageWithURL:[NSURL URLWithString:item.user.avatarUrl]
                     placeholderImage:[UIImage imageNamed:@"userPlaceholder"]];

}

@end
