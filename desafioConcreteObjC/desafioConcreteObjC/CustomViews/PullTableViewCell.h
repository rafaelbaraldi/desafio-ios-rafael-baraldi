//
//  PullTableViewCell.h
//  desafioConcreteObjC
//
//  Created by Rafael Baraldi on 10/04/16.
//  Copyright © 2016 Rafael BAraldi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRequestsDataModels.h"

@interface PullTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblOwner;

@property (weak, nonatomic) IBOutlet UIImageView *imgOwner;


-(void)fillCellWithItem:(GHPullRequestsResponse*)item;

@end
