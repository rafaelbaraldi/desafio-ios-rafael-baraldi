//
//  RepoTableViewCell.m
//  desafioConcreteObjC
//
//  Created by Rafael Baraldi on 10/04/16.
//  Copyright © 2016 Rafael BAraldi. All rights reserved.
//

#import "RepoTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation RepoTableViewCell

-(void)fillCellWithItem:(GHItems*)item{
    
    self.lblRepoName.text = item.name;
    self.lblRepoDesc.text = item.itemsDescription;
    self.lblRepoBranchs.text = [NSString stringWithFormat:@"Forks: %.0f", item.forks];
    self.lblRepoStars.text = [NSString stringWithFormat:@"Stars: %.0f", item.stargazersCount];
    
    self.lblOwnerName.text = item.owner.login;
    
    [self.imgOwner sd_setImageWithURL:[NSURL URLWithString:item.owner.avatarUrl]
                     placeholderImage:[UIImage imageNamed:@"userPlaceholder"]];
}

@end
