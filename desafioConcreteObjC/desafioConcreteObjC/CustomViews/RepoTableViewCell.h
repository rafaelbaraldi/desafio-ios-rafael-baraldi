//
//  RepoTableViewCell.h
//  desafioConcreteObjC
//
//  Created by Rafael Baraldi on 10/04/16.
//  Copyright © 2016 Rafael BAraldi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepositoriesDataModels.h"

@interface RepoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblRepoName;
@property (weak, nonatomic) IBOutlet UILabel *lblRepoDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblRepoBranchs;
@property (weak, nonatomic) IBOutlet UILabel *lblRepoStars;
@property (weak, nonatomic) IBOutlet UILabel *lblOwnerName;
@property (weak, nonatomic) IBOutlet UILabel *lblOwnerLastName;

@property (weak, nonatomic) IBOutlet UIImageView *imgOwner;

-(void)fillCellWithItem:(GHItems*)item;

@end
